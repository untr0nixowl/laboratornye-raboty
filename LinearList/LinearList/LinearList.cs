﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinearList
{
    public class LinearList<T> : IList<T> where T: IComparable<T>
    {

        #region Private Members
        private class LinearItem
        {
            public T Value { get; set; }
            public LinearItem Next { get; set; }
        }

        private LinearItem _head;
        private int _count;
        private bool _isReadOnly;
        #endregion

        #region Constructors
        public LinearList(bool isReadOnly = false) {
            _count = 0;
            _isReadOnly = isReadOnly;

        }
        #endregion


        #region Accessors
        public T this[int index]
        {
            get
            {
                if (index >= Count || index < 0)
                    throw new System.ArgumentException("Your index out of range");
                var current = _head;
                for (int idx = 0; idx < index && current != null; ++idx, current = current.Next) ;
                return current.Value;
            }
            set
            {
                if (index >= Count || index < 0)
                    throw new System.ArgumentException("Your index is more than size of list");
                var current = _head;
                for (int idx = 0; idx < index && current != null; ++idx, current = current.Next) ;
                current.Value = value;

            }
        }

        public int Count => _count;

        public bool IsReadOnly =>_isReadOnly;
        #endregion

        #region Methods
        public void Add(T item)
        {
            /*
            if (_head == null)
                _head = new LinearItem { Value = item, Next = null };
            else
            {
                var current = _head;
                while (current != null && current.Next != null)
                {
                    current = current.Next;
                }
                var newItem = new LinearItem { Value = item, Next = current.Next };
                current.Next = newItem;
            }
            _count++;
            */
            Insert(Count, item);
        }

        public void Clear()
        {
            if (_head == null)
                return;
            for (int idx = Count - 1; idx  >= 0; --idx)
                RemoveAt(idx);
        }

        public bool Contains(T item)
        {
            if (_head == null)
                return false;
            var current = _head;
            while (current != null)
            {
                if (current.Value.CompareTo(item) == 0)
                    return true;
                current = current.Next;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var current = _head;
            for (int idx = arrayIndex; idx < array.Length && current != null; ++idx, current = current.Next)
                array[idx] = current.Value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var current = _head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
            
        }

        public int IndexOf(T item)
        {
            if (_head == null)
                throw new System.ArgumentException("List is Empty");
            int index = 0;
            var current = _head;
            while (current != null) {
                if (current.Value.CompareTo(item) == 0)
                    return index;
                current = current.Next;
                index++;
            }
           throw new System.ArgumentException("Element not found");
        }

        public void Insert(int index, T item)
        {
            if (index > Count || index < 0)
                throw new System.ArgumentException("Your index out of range");
            if (index == 0)
            {
                var newNode = new LinearItem { Value = item, Next = _head };
                _head = newNode;
            }
            else
            {
                var current = _head;
                int idx = 0;
                while (current.Next != null && idx != index - 1)
                    current = current.Next;
                var newNode = new LinearItem { Value = item, Next = current.Next };
                current.Next = newNode;
            }
            ++_count;

        }

        public bool Remove(T item)
        {
            if (_head == null)
                return false;
            var current = _head;
            try
            {
                var index = IndexOf(item);
                RemoveAt(index);
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public void RemoveAt(int index)
        {
            if (index >= Count || index < 0)
                throw new System.ArgumentException("Your index out of range");
            if (index == 0)
            {
                _head = _head.Next;
            }
            else
            {
                var current = _head;
                for (int idx = 0; idx < index - 1 && current != null; ++idx, current = current.Next) ;
                current.Next = current.Next.Next;
            }
            --_count;

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return string.Join(",", this);
        }
        #endregion
    }
}
