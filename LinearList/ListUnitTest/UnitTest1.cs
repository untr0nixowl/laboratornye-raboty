using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinearList;
using System;

namespace ListUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private LinearList<int> GenerateList(int size)
        {
            var list = new LinearList<int>();
            for (int idx = 0; idx < size; ++idx)
                list.Add(idx);
            return list;

        }

        [TestInitialize]
        public void TestCreateList()
        {
            var list = new LinearList<int>(true);
        }


        [TestMethod()]
        public void TestInsertOneItemInList()
        {
            var list = new LinearList<int>();
            list.Insert(0, 0);
            Assert.AreEqual("0", list.ToString());

        }
        [TestMethod()]
        public void TestInsertTenItemsInList()
        {
            var list = new LinearList<int>();
            for (int idx = 0; idx < 10; ++idx)
                list.Insert(idx, idx);
            Assert.AreEqual("0,1,2,3,4,5,6,7,8,9", list.ToString());
        }
        [TestMethod()]
        public void TestInsertOneMillionItemsInList()
        {
            var list = new LinearList<int>();
            for (int idx = 0; idx < 1000000; ++idx)
                list.Insert(idx, idx);
            var array = new int[100000];
            for (int idx = 0; idx < 1000000; ++idx)
                array[idx] = idx;
            Assert.AreEqual(string.Join(",", array), list.ToString());

        }
        [TestMethod()]
        public void TestAddOneElement()
        {
            Assert.AreEqual("0", GenerateList(1).ToString());
        }
        [TestMethod()]
        public void TestAddOneHundredElement()
        {
            var array = new int[100];
            for (int idx = 0; idx < 100; ++idx)
                array[idx] = idx;
            Assert.AreEqual(string.Join(",", array), GenerateList(100).ToString());

        }
        [TestMethod()]
        public void TestAddOneMillionElement()
        {
            var array = new int[100000];
            for (int idx = 0; idx < 100000; ++idx)
                array[idx] = idx;
            Assert.AreEqual(string.Join(",", array), GenerateList(100000).ToString());

        }

        [TestMethod]
        public void TestRemoveOneElement()
        {
            var list = new LinearList<int>();
            list.Insert(0, 0);
            list.Remove(0);
        }
        [TestMethod]
        public void TestRemoveTenElements()
        {
            var list = GenerateList(10);
            for (int idx = 0; idx < 10; ++idx)
                list.Remove(idx);
            Assert.AreEqual(0, list.Count);
        }
        [TestMethod]
        public void TestRemoveOneMillionElements()
        {
            var list = GenerateList(1000000);
            for (int idx = 0; idx < 1000000; ++idx)
                list.Remove(idx);
            Assert.AreEqual(0, list.Count);
        }
        [TestMethod]
        public void TestAccessToEmptyArray()
        {
            var list = new LinearList<int>();
            Assert.ThrowsException<System.ArgumentException>(() => list[1]);
        }
        [TestMethod]
        public void DeleteEmptyList()
        {
            var list = new LinearList<int>();
            Assert.ThrowsException<System.ArgumentException>(() => list.RemoveAt(1));
        }
        [TestMethod()]
        public void TestDeleteOneElements()
        {
            var list = GenerateList(1);
            list.RemoveAt(0);
            Assert.AreEqual("", list.ToString());
        }
        [TestMethod()]
        public void TestDeleteTenElements()
        {
            var list = GenerateList(10);
            int count = 9;
            while (count != -1)
            {
                list.RemoveAt(count);
                count--;
            }
            Assert.AreEqual("", list.ToString());
        }
        [TestMethod()]
        public void TestDeleteHundredElements()
        {
            var list = GenerateList(100);
            int count = 99;
            while (count != -1)
            {
                list.RemoveAt(count);
                count--;
            }
            Assert.AreEqual("", list.ToString());
        }
        [TestMethod()]
        public void TestDeleteHundredsElements()
        {
            var list = GenerateList(100000);
            int count = 99999;
            while (count != -1)
            {
                list.RemoveAt(count);
                count--;
            }
            Assert.AreEqual("", list.ToString());
        }

        [TestMethod()]
        public void TestFindObject()
        {
            var list = GenerateList(10);
            Assert.AreEqual(9, list.IndexOf(9));
        }
        [TestMethod()]
        public void FindObjectIn10000()
        {
            var list = GenerateList(10000);
            Assert.AreEqual(5000, list.IndexOf(5000));
        }

        [TestMethod()]
        public void TestFindObjectExistence()
        {
            var list = GenerateList(100000);
            Assert.AreEqual(true, list.Contains(99899));
        }

        [TestMethod()]
        public void TestClear()
        {
            var list = GenerateList(100000);
            list.Clear();
            Assert.AreEqual("", list.ToString());
        }

        [TestMethod]
        public void TestCopyTo()
        {
            var array = new int[100000];
            var result = new int[100000];
            var list = GenerateList(100000);
            for(int idx = 0;idx < 100000; ++idx)
            {
                result[idx] = idx;
            }
            list.CopyTo(array, 0);
            Assert.AreEqual(string.Join(",", array),string.Join(",",result));
        }

        [TestMethod]
        public void TestCopyPartiesTo()
        {
            var array = new int[200000];
            var result = new int[200000];
            var list = GenerateList(100000);
            int count = 0;
            for (int idx = 100000; idx < 200000; ++idx)
            {
                result[idx] = count;
                count++;
            }
            list.CopyTo(array, 100000);
            Assert.AreEqual(string.Join(",", array), string.Join(",", result));
        }

    }
}
